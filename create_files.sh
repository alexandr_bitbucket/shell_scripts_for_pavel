#!/bin/sh

INPUT_FILE=$1

if [ ! -f "$INPUT_FILE" ]; then
  echo "$INPUT_FILE not a regular file"
  exit 1
fi

function translit {
LINE=`echo $LINE | sed 'y/абвгдезиклмнопрстуфцыАБВГДЕЗИКЛМНОПРСТУФЦЫ/abvgdeziklmnoprstufcyABVGDEZIKLMNOPRSTUFCY/
s/ё/jo/
s/Ё/JO/
s/ж/zh/
s/Ж/ZH/
s/й/jj/
s/Й/JJ/
s/х/kh/
s/Х/KH/
s/ч/ch/
s/Ч/CH/
s/ш/sh/
s/Ш/SH/
s/щ/shh/
s/Щ/SHH/
s/э/eh/
s/Э/EH/
s/ю/ju/
s/Ю/JU/
s/я/ja/
s/Я/JA/
s/ь/_/
s/Ь/_/
s/ъ/_/
s/Ъ/_/'` 
}

function clean {
  LINE=`echo $LINE | sed 's/[ \/]/_/g'`
}


while read -r LINE
do
  translit
  clean
  touch $LINE
done < $INPUT_FILE
 
