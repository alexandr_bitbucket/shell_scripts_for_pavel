#!/bin/bash

WORKDIR=${1:-"."}

echo -n > list_files.log
find $WORKDIR -type f | while read FILE; do
  echo `basename $FILE | cut -d'.' -f1`";"`basename $(dirname $FILE)`";$FILE" >> list_files.log
done

