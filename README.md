### create_files.sh ###

```
#!bash
create_files.sh file_with_lines
```
для каждой строки из file_with_lines проводит транслитерацию, заменяет пробелы и "/". После чего создаёт пустой файл с получившимся именем.

### add_to_files_in_directory.sh ###
```
#!bash
add_to_files_in_directory.sh file directory
```
Содержимое файла file добавляет в конец каждого файла, расположенного в directory.

### replace_pattern.sh ###
```
#!bash
replace_pattern.sh file.html
```
Заменяет внутри файла file.html шаблон "REPLACE_ME" на имя файла без ".html". 

### translit_dirs_and_files.sh ###
```
#!bash
translit_dirs_and_files.sh directory
```
Для всех директорий и файлов в directory производит транслитерацию имён. Для переименованных файлов дополнительно ведётся журнал в translit_dirs_and_files.log
Если не задано directory, то работает в текущей директории.

### translit_dirs_and_files.test.sh ###
```
#!bash
translit_dirs_and_files.test.sh
```
Создаёт тестовые директории/файлы и запускает translit_dirs_and_files.sh

### list_files.sh ###
```
#!bash
list_files.sh directory
```
Для всех директорий и файлов в directory создаёт список (как скрипт translit_dirs_and_files.sh) list_files.log
Если не задано directory, то работает в текущей директории.
Внимание! Не способен корректно отработать директорию, в которой есть имена файлов с пробелами. Это не проблема, если предварительно запускать translit_dirs_and_files.sh


### FM-mix.py ###

Генерирует все возможные сочетания параметров пола, цвета, качества и размера, например "FSAS20".

Пол:

* F - Самки
* M - Самцы

Цвет:

* BL - Чёрные 
* SA - Сапфир
* SB - Серебристо голубые
* PA - пастелевые
* WT - белые
* PV - гибриды пастелевые

Качество:

* N - Норма
* S - Малый дефект 
* M - Средний дефект 
* L - Большой дефект 
* X - Брак (особый размер)

Размеры:

* Самцы: 40+, ‎40, 30, 20, 0, 1, 2, 3, 4
* Самки: ‎20, 0, 1, 2, 3, 4
* Брак: 25, 15, 7, 3, 1

### admin_log_to_found_list.sh ###

Читает строки из файла журнала работы пула `admin_log.txt` и из строк, сообщающих о найденом блоке делает новый файл `found.part`.

Пример входных данных:
```
2017-11-27 20:55:02 (Thread 2) Accepted trusted share at difficulty 100/105 from KaycrDrxAMnKfCbQTLZHpQ58JJUvghM46UuKwRj5yqkaHeSAcVgPgjfe9P8Sk63rNjXGMsA8Xrnde5VJbmDrq16f9GQFT2o@10.100.3.100
2017-11-27 20:55:02 (Thread 2) Accepted trusted share at difficulty 100/464 from KaycrDrxAMnKfCbQTLZHpQ58JJUvghM46UuKwRj5yqkaHeSAcVgPgjfe9P8Sk63rNjXGMsA8Xrnde5VJbmDrq16f9GQFT2o@10.100.3.100
2017-11-27 20:55:05 (Thread 1) New block to mine at height 173161 w/ difficulty of 1309672918
2017-11-27 20:55:03 (Thread 2) Accepted trusted share at difficulty 100/141 from KaycrDrxAMnKfCbQTLZHpQ58JJUvghM46UuKwRj5yqkaHeSAcVgPgjfe9P8Sk63rNjXGMsA8Xrnde5VJbmDrq16f9GQFT2o@10.100.3.100
2017-11-27 20:55:04 (Thread 2) Block e83a8f found at height 173160 by miner KaycrDrxAMnKfCbQTLZHpQ58JJUvghM46UuKwRj5yqkaHeSAcVgPgjfe9P8Sk63rNjXGMsA8Xrnde5VJbmDrq16f9GQFT2o@10.11.17.80 - submit result: {"status":"OK"}
2017-11-27 20:55:04 (Thread 2) Accepted valid share at difficulty 200000/4428182165 from KaycrDrxAMnKfCbQTLZHpQ58JJUvghM46UuKwRj5yqkaHeSAcVgPgjfe9P8Sk63rNjXGMsA8Xrnde5VJbmDrq16f9GQFT2o@10.11.17.80
2017-11-27 20:55:06 (Thread 2) Accepted trusted share at difficulty 100/141 from KaycrDrxAMnKfCbQTLZHpQ58JJUvghM46UuKwRj5yqkaHeSAcVgPgjfe9P8Sk63rNjXGMsA8Xrnde5VJbmDrq16f9GQFT2o@10.100.3.100
2017-11-27 20:55:06 (Thread 2) New block to mine at height 173161 w/ difficulty of 1309672918
2017-11-27 20:55:06 (Thread 2) Accepted trusted share at difficulty 100/195 from KaycrDrxAMnKfCbQTLZHpQ58JJUvghM46UuKwRj5yqkaHeSAcVgPgjfe9P8Sk63rNjXGMsA8Xrnde5VJbmDrq16f9GQFT2o@10.100.3.100
2017-11-27 20:55:08 (Thread 2) Accepted trusted share at difficulty 100/130 from KaycrDrxAMnKfCbQTLZHpQ58JJUvghM46UuKwRj5yqkaHeSAcVgPgjfe9P8Sk63rNjXGMsA8Xrnde5VJbmDrq16f9GQFT2o@10.100.3.100
2017-12-02 16:03:27 (Thread 2) Accepted trusted share at difficulty 100/606 from KaycrDrxAMnKfCbQTLZHpQ58JJUvghM46UuKwRj5yqkaHeSAcVgPgjfe9P8Sk63rNjXGMsA8Xrnde5VJbmDrq16f9GQFT2o@10.100.3.100
2017-12-02 16:03:28 (Thread 2) Block 217c0e found at height 174617 by miner KaycrDrxAMnKfCbQTLZHpQ58JJUvghM46UuKwRj5yqkaHeSAcVgPgjfe9P8Sk63rNjXGMsA8Xrnde5VJbmDrq16f9GQFT2o@10.11.17.80 - submit result: {"status":"OK"}
2017-12-02 16:03:28 (Thread 2) Accepted trusted share at difficulty 200000/1992299055 from KaycrDrxAMnKfCbQTLZHpQ58JJUvghM46UuKwRj5yqkaHeSAcVgPgjfe9P8Sk63rNjXGMsA8Xrnde5VJbmDrq16f9GQFT2o@10.11.17.80
2017-12-02 16:03:28 (Thread 2) Accepted trusted share at difficulty 100/534 from KaycrDrxAMnKfCbQTLZHpQ58JJUvghM46UuKwRj5yqkaHeSAcVgPgjfe9P8Sk63rNjXGMsA8Xrnde5VJbmDrq16f9GQFT2o@10.100.3.100
```

Пример выходных данных:
```
<tr><td>at height 173160</td><td>by miner KaycrDrx...f9GQFT2o</td></tr>
<tr><td>at height 174617</td><td>by miner KaycrDrx...f9GQFT2o</td></tr>
```