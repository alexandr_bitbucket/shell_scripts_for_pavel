#!/usr/bin/env bash

TEMPLATE='<tr><td>at height \1</td><td>by miner \2</td></tr>'
FILE='/root/admin_log/admin_log.txt'
PATTERN='found at'
OUTPUT='/root/admin_log/found.part'

if [ ! -f $FILE ]; then
  echo "File: $FILE not found. Exiting"
  exit 1
fi

cat $FILE | grep "$PATTERN" | sed -re "s|([^ ]{8})[^ @]+([^ @]{8})@|\1...\2@|" | sed -re "s|.*height ([0-9]+) by miner ([^ @]+)@.*|$TEMPLATE|" > $OUTPUT
