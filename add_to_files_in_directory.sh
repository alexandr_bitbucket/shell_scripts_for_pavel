#!/bin/bash

SOURCE_FILE=$1
DIRECTORY=$2

if [ ! -f "$SOURCE_FILE" ]; then
  echo "$SOURCE_FILE not a regular file"
  exit 1
fi

if [ ! -d "$DIRECTORY" ]; then
  echo "$DIRECTORY not a directory"
  exit 1
fi

CONTENT=`cat $SOURCE_FILE`

FILES=`find ./$DIRECTORY -type f`

for FILE in $FILES; do
  echo "$CONTENT" >> $FILE
done

