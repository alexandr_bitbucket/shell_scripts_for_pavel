#!/bin/bash

LOGFILE="`pwd`/translit_dirs_and_files.log"
WORKDIR=${1:-"."}

function _addZero() {
RET="$1"
# "новая/Новая папка (1)" -> "новая/Новая папка (001)"
RET=`echo $RET | sed 's/(\([0-9]\))/(00\1)/'`
# "новая/Новая папка (11)" -> "новая/Новая папка (011)"
RET=`echo $RET | sed 's/(\([0-9][0-9]\))/(0\1)/'`
# "новая/Новая папка (111)" -> "новая/Новая папка (111)"
RET=`echo $RET | sed 's/(\([0-9][0-9][0-9]\))/(\1)/'`
echo $RET
}

function _replaceSpaces() {
echo "$1" | sed 's/ /_/g'
}

function _replaceBrackets() {
echo "$1" | sed 's/[\(\)]/_/g'
}

function _removeDubledUnderscores() {
echo "$1" | sed -r 's/_[_]+/_/g'
}

function _removeEndingUnderscore() {
echo "$1" | sed -r 's/_+\././g
s/_+$//g'
}

function _toLower() {
echo "$1" | awk '{print tolower($0)}'
}

function _translit() {
echo $1 | sed 'y/абвгдезиклмнопрстуфцыАБВГДЕЗИКЛМНОПРСТУФЦЫ/abvgdeziklmnoprstufcyabvgdeziklmnoprstufcy/
s/ё/jo/g
s/Ё/jo/g
s/ж/zh/g
s/Ж/zh/g
s/й/jj/g
s/Й/jj/g
s/х/kh/g
s/Х/kh/g
s/ч/ch/g
s/Ч/ch/g
s/ш/sh/g
s/Ш/sh/g
s/щ/shh/g
s/Щ/shh/g
s/э/eh/g
s/Э/eh/g
s/ю/ju/g
s/Ю/ju/g
s/я/ja/g
s/Я/ja/g
s/ь/_/g
s/Ь/_/g
s/ъ/_/g
s/Ъ/_/g'
}

function getNewName() {
  RET="$1"
  RET=$(_addZero "$RET")
  RET=$(_translit "$RET")
  RET=$(_replaceSpaces "$RET")
  RET=$(_replaceBrackets "$RET")
  RET=$(_removeDubledUnderscores "$RET")
  RET=$(_removeEndingUnderscore "$RET")
  RET=$(_toLower "$RET")
  echo $RET
}

find $WORKDIR -depth -type d | while read DIR; do
  pushd "$DIR" > /dev/null
  DIR_TRANSLIT=$(getNewName "$DIR")
  BASEDIR=`basename "$DIR"`
  BASEDIR_TRANSLIT=$(getNewName "$BASEDIR")

  find -type f | while read FILE; do
    # "./Помидоры.jpg" -> Помидоры.jpg
    FILE=`echo "$FILE" | xargs --null basename`
    FILE_TRANSLIT=$(getNewName "$FILE")


    FILE_WO_EXTENSION=`echo "$FILE" | cut -d'.' -f1`
    FILE_WO_EXTENSION_AND_BRACKETS=`echo "$FILE" | cut -d'.' -f1 | sed 's/[\(\)]//g'`
    FILE_WO_EXTENSION_TRANSLIT=$(getNewName "$FILE_WO_EXTENSION")

    if [ "$FILE" != "$FILE_TRANSLIT" ]; then
      mv "$FILE" "$FILE_TRANSLIT"

  #pomidory;ovoshhi_krasnye;./tmp/ovoshhi/ovoshhi_krasnye/pomidory.jpg;"овощи красные";"помидоры"
      echo "$FILE_WO_EXTENSION_TRANSLIT;\
$BASEDIR_TRANSLIT;\
$DIR_TRANSLIT/$FILE_TRANSLIT;\
\"$BASEDIR\";\
\"$FILE_WO_EXTENSION_AND_BRACKETS\"" >> $LOGFILE

    fi
   
  done
  cd ..

  if [ "$BASEDIR" != "$BASEDIR_TRANSLIT" ]; then
    mv "$BASEDIR" "$BASEDIR_TRANSLIT"
  fi

  popd > /dev/null
done
