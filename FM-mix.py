
m_sizes = ["40+", "40", "30", "20", "0", "1", "2", "3", "4"]
f_sizes = ["20", "0", "1", "2", "3", "4"]
x_sizes = ["25", "15", "7", "3", "1"]

sexes = ["F", "M"]
colors = ["BL", "SA", "SB", "PA", "WT", "PV"]
qualities = ["N", "S", "M", "L", "X"]

counter = 0
for sex in sexes:
    for color in colors:
        for quality in qualities:
            sizes = []
            if quality == "X":
                sizes = x_sizes
            elif sex == "F":
                sizes = f_sizes
            elif sex == "M":
                sizes = m_sizes
            else:
                print('WHAT???')
                exit(1)
            for size in sizes:
                counter += 1
                print("%s%s%s%s" % (sex, color, quality, size))

# print(counter)
