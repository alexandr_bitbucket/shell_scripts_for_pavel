#!/bin/sh

SOURCE_FILE=$1

if [ ! -f "$SOURCE_FILE" ]; then
  echo "$SOURCE_FILE not a regular file"
  exit 1
fi

FILE=`basename $SOURCE_FILE .html`

sed -i "s/REPLACE_ME/$FILE/g" $SOURCE_FILE
